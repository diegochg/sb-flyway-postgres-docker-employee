CREATE TABLE Employee(
	id bigint PRIMARY KEY,
	name varchar(30) NOT NULL,
	mail varchar(40) NOT NULL
);

CREATE SEQUENCE sq_employee_id_pk2
        INCREMENT BY 1
        MINVALUE 1
        MAXVALUE 9999999999
        START 1
        NO CYCLE;

ALTER SEQUENCE sq_employee_id_pk2 OWNED BY Employee.id;