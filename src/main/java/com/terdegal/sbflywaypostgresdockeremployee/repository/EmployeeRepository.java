package com.terdegal.sbflywaypostgresdockeremployee.repository;

import com.terdegal.sbflywaypostgresdockeremployee.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
