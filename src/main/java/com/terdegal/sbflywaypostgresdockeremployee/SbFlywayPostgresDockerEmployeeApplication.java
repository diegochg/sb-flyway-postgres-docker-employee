package com.terdegal.sbflywaypostgresdockeremployee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbFlywayPostgresDockerEmployeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbFlywayPostgresDockerEmployeeApplication.class, args);
	}

}
