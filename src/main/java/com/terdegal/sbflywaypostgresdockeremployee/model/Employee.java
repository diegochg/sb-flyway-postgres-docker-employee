package com.terdegal.sbflywaypostgresdockeremployee.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "Employee")
public class Employee implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
  @SequenceGenerator(sequenceName = "sq_employee_id_pk2", allocationSize = 1, name = "SEQ")
  @Column(nullable = false, updatable = false)
  private Long id;

  private String name;
  private String mail;

}
