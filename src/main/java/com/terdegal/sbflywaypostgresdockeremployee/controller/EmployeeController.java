package com.terdegal.sbflywaypostgresdockeremployee.controller;

import com.terdegal.sbflywaypostgresdockeremployee.model.Employee;
import com.terdegal.sbflywaypostgresdockeremployee.repository.EmployeeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

  @Autowired
  private EmployeeRepository employeeRepository;

  @GetMapping("/all")
  public ResponseEntity<List<Employee>> getAllEmployees() {
    return new ResponseEntity<List<Employee>>(employeeRepository.findAll(), HttpStatus.OK);
  }

  @PostMapping("/new")
  public ResponseEntity<String> saveEmployee(@RequestBody Employee employee) {
    employeeRepository.save(employee);
    return new ResponseEntity<String>("Employee created", HttpStatus.CREATED);
  }

}
