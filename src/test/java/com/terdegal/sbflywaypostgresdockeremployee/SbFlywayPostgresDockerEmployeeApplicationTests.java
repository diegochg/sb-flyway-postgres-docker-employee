package com.terdegal.sbflywaypostgresdockeremployee;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = SbFlywayPostgresDockerEmployeeApplicationTests.class)
class SbFlywayPostgresDockerEmployeeApplicationTests {

  @Test
  void contextLoads() {
  }

}
